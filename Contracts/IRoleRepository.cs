﻿using HRPM.Entities.Helpers;
using HRPM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HRPM.Contracts
{
   public interface IRoleRepository : IRepositoryBase<Role>
    {
		PagedList<Role> GetRole(PagingParameters pageParameters);
		Role GetRoleById(int RoleId);
		void CreateRole(Role role);
		void UpdateRole(Role DbRole, Role role);
		void DeleteRole(Role role);

	}
}
