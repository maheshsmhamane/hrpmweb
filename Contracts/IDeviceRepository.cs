﻿using HRPM.Entities.Helpers;
using HRPM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace HRPM.Contracts
{
   public interface IDeviceRepository : IRepositoryBase<Device>
    {
		PagedList<Device> GetDevice(DeviceParameters ownerParameters);
		Device GetDeviceById(int deviceId);
		void CreateDevice(Device device);
		void UpdateDevice(Device dbDevice, Device device);
		void DeleteDevice(Device device);

	}
}
