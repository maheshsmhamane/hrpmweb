﻿using HRPM.Entities.Helpers;
using HRPM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HRPM.Contracts
{
   public interface IOrganizationRepository: IRepositoryBase<Organization>
    {
		PagedList<OrganizationViewModel> GetOrganization(OrganizationParameters pageParameters);
		Organization GetOrganizationById(int OganizationId);
		void CreateOrganization(Organization organization);
		void UpdateOrganization(Organization Dborganization, Organization organization);
		void DeleteOrganization(Organization organization);

	}
}
