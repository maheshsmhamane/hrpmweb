﻿using HRPM.Entities.Helpers;
using HRPM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HRPM.Contracts
{
   public interface IPatientHealthReadingRepository : IRepositoryBase<PatientHealthReading>
    {
		PagedList<PatientHealthReading> GetPatientHealthReading(int DeviceId,PagingParameters pageParameters);
		PatientHealthReading GetPatientHealthReadingById(int DeviceId,int patientHealthId);
		void CreatePatientHealthReading(PatientHealthReading patientHealthReading);
		void UpdatePatientHealthReading(PatientHealthReading DbpatientHealthReading, PatientHealthReading patientHealthReading);
		void DeletePatientHealthReading(PatientHealthReading patientHealthReading);

	}
}
