﻿using HRPM.Entities.Helpers;
using HRPM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HRPM.Contracts
{
   public interface IPatientDeviceAssignmentRepository : IRepositoryBase<PatientDeviceAssignment>
    {
		PagedList<PatientDeviceAssignment> GetPatientDeviceAssignment(int PatientId,PatientDeviceAssignmentParameters patientDeviceAssignmentParameters);
		PatientDeviceAssignment GetPatientDeviceAssignmentById(int PatientId,int PatientDeviceAssignmentId);
		void CreatePatientDeviceAssignment(PatientDeviceAssignment patientDeviceAssignment);
		void UpdatePatientDeviceAssignment(PatientDeviceAssignment DbPatientDeviceAssignment, PatientDeviceAssignment patientDeviceAssignment);
		void DeletePatientDeviceAssignment(PatientDeviceAssignment patientDeviceAssignment);

	}
}
