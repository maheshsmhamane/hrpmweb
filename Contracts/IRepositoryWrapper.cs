﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HRPM.Contracts
{
    public interface IRepositoryWrapper
    {
       
        IDeviceRepository Device { get; }
        IPatientRepository Patient { get; }
        IOrganizationRepository Organization { get; }
        IPatientDeviceAssignmentRepository PatientDeviceAssignment { get; }
        IPatientHealthReadingRepository PatientHealthReading { get; }
        IUserRepository User { get; }
        IRoleRepository Role { get; }
        void Save();
	}
}
