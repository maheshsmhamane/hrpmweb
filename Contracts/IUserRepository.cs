﻿using HRPM.Entities.Helpers;
using HRPM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HRPM.Contracts
{
   public interface IUserRepository: IRepositoryBase<User>
    {
		PagedList<UserViewModel> GetUser(int OrganizationId,UserParameters userParameters);
		PagedList<UserViewModel> GetUser(UserParameters userParameters);
		User GetUserById(int Orgnization,int UserID);
		User GetUserById(int UserID);
		void CreateUser(User  user);
		void UpdateUser(User Dbuser, User user);
		void DeleteUser(User user);

	}
}
