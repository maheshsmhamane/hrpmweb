﻿using System;
using System.Collections.Generic;
using System.Text;
using HRPM.Entities.Helpers;
using HRPM.Entities.Models;

namespace HRPM.Contracts
{
   public interface IPatientRepository : IRepositoryBase<Patient>
    {
		PagedList<PatientsViewModel> GetPatient(PatientParameters ownerParameters);
		PagedList<PatientsViewModel> GetPatient(int organizationId,PatientParameters ownerParameters);
		Patient GetPatientById(int patientId);
		void CreatePatient(Patient patients);
		void UpdatePatient(Patient dbpatients, Patient patients);
		void DeletePatient(Patient patients);
	}
}
