﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace HRPM.Entities.Enumerations
{
    public enum UserType
    {
        //[EnumMember(Value = "Admin")]
        Admin =0,
       // [EnumMember(Value = "Manager")]
        Manager =1,
       // [EnumMember(Value = "User")]
        User =2,
//[EnumMember(Value = "DataConsumer")]
        DataConsumer =3,
    //    [EnumMember(Value = "Doctor")]
        Doctor = 4
    }
}
