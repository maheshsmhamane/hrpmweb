﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Entities.Enumerations
{
    public enum UnitsOfMeasurement :byte
	{
		[Description("KG")] //Weight
		Pound = 0,

		[Description("F")]//Temprature
		Fahrenheit = 1,

		[Description("mg/dL")] //Glucometers mg/dL or mmol/L.
		MiliGram = 2,

		[Description("mmHg")] //BloodPessure
		MiliMeters = 3,

		[Description("SpO2")]
		PulseOximeters = 4,

		[Description("KM")]  //(Miles or Kilometers.)(ActivityTracker)
		Kilometers = 5

	}
}
//MASUREMNT OF UNTIS
//		UnitsOfMeasurement .Table
//		Pound		(Weight scale)
//		Fahrenheit 	(Temprature)
//		MiliGram 	(mg/dL or mmol/L.) (Glucometers)
//MiliMeters 	(millimeters of mercury (mmHg))(BloodPessure)
//		SpO2  		(peripheral oxygen saturation (SpO2))(PulseOximeters)
//Kilometers	(Miles or Kilometers.)(ActivityTracker)
//(peripheral oxygen saturation (SpO2))(PulseOximeters