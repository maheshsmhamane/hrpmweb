﻿using HRPM.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace HRPM.Entities
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Role> Roles { get; set; }

        public DbSet<Device> Devices { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<PatientDeviceAssignment> PatientDeviceAssignments { get; set; }
        public DbSet<PatientHealthReading> PatientHealthReadings { get; set; }
        public DbSet<ActivityLog> ActivityLogs { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

        }
    }
}
