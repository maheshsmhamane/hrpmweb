﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using HRPM.Entities.Enumerations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRPM.Entities.Models
{
    public class Role:BaseModel,IEntity
    {
        [Key]
        [Column("RoleId")]
        public int Id { get; set; }
        //public string RoleTypeEnum => RoleType.ToString();

        [Column(TypeName = "nvarchar(max)")]
        [Display(Name ="Role")]
        [Required]
        public string RoleType { get; set; }


    }
}
