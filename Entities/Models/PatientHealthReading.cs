﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using HRPM.Entities.Enumerations;
namespace HRPM.Entities.Models
{
    public class PatientHealthReading :IEntity
    {

        [Key]
        [Column("PatientHealthId")]
        public int Id { get; set; }
        public Patient Patients{get; set;}

        [ForeignKey("DeviceId")]
        [Required]
        public int DeviceId { get; set; }
        public Device Device { get; set; }
        [Required]
        
        public float ReadingValue { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTimeOffset? CapturedOn { get; set; }

        [Required]
        public string CreatedBy { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime CreatedOn { get; set; }

    }

}


//public int Weight { get; set; }


//public string Temprature { get; set; }

//public string BloodPressure { get; set; }

//public string PulseOximeters { get; set; }

//public string Glucometers { get; set; }

//public string ActivityTracker { get; set; }
//public string ECG { get; set; }