﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HRPM.Entities.Models
{
   public class PatientsViewModel
    {
        public string Name { get; set; }
 
        public string Address { get; set; }
        public string Gender { get; set; }
        public string Mobile { get; set; }
        public string OrganizationName { get; set; }
        public string User { get; set; }
        public string Status { get; set; }
        public int PatientId { get; set; }
        public int OrganizationId { get; set; }
        public int UserId {get;set;}
        

    }
}
