﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Entities.Models
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Role { get; set; }
        public int RoleId { get; set; }
        public string Status { get; set; }
        public string OrganizationName { get; set; }
        public int OrganizationId { get; set; }

    }
}
