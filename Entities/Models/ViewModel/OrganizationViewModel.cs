﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Entities.Models
{
    public class OrganizationViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string phone { get; set; }
        public int ZipCode { get; set; }
        public string OwnerName { get; set; }
        public string OwnerContact { get; set; }

        // public int MyProperty { get; set; }


    }
}
