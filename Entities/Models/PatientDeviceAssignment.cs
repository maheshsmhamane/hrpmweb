﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRPM.Entities.Models
{
    public class PatientDeviceAssignment : BaseModel,IEntity
    {

        [Key]
        [Column("PatientDeviceAssignmentId")]
        public int Id { get; set; }
        
        [Required]                                  
        public DateTime AssignedDate { get; set; }
        [Required]
        public DateTime UnAssignedDate { get; set; }
        
        [ForeignKey("DeviceId")]
        [Required]
        public int DeviceId { get; set; }
        public Device Device { get; set; }


        [ForeignKey("PatientId")]
        public int PatientId { get; set; }
        public Patient Patient { get; set; }

    }
}
