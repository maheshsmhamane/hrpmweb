﻿namespace HRPM.Entities.Models
{
	public class DeviceParameters : QueryStringParameters
	{
		public string Name { get; set; }
	}
}
