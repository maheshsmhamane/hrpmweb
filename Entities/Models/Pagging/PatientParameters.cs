﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HRPM.Entities.Models
{
   public  class PatientParameters:QueryStringParameters
    {
        public string Name { get; set; }
        public string Status { get; set; } = "Active";
        public string Orgnization { get; set; } = "All Orgnizations";  //on userdetails screen dropdown for selecting organization

    }
}
