﻿namespace HRPM.Entities.Models
{
	public class OrganizationParameters : QueryStringParameters
	{
		
		public string Name { get; set; }
		public string Status { get; set; }
		public string Organization { get; set; }

	}
}
