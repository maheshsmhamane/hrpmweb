﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HRPM.Entities.Models
{
    public class UserParameters:QueryStringParameters
    {
        public string Name { get; set; }
        public string Organization { get; set; } = "All Organizations";  //on userdetails screen dropdown for selecting organization
        public string Status { get; set; } = "Active";
    }
}
