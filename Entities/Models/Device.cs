﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HRPM.Entities.Models
{
    public class Device : BaseModel,IEntity
    {

        [Key]
        [Column("DeviceId")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter Device Name")]
        //[Display(Name = "Device Name")]
        [StringLength(100)]
        [RegularExpression(@"^[a-zA-Z]+$")]
        [DataType(DataType.Text)]
        public string DeviceName { get; set; }
        [Required]
        public string DeviceType { get; set; }
        [Required]
        [Column(TypeName = "bit")]
        public bool IsAssigned { get; set; }
        [Required]
        [Column(TypeName = "bit")]
        public bool IsAuthenticated { get; set; }
        
        public float UpperLimit { get; set; }
        public float LowerLimit { get; set; }

    }
}
