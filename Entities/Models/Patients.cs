﻿using HRPM.Entities.Enumerations;
using HRPM.Entities.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Entities.Models
{
    public class Patient : BaseModel,IEntity
    {
        [Key]
        [Column("PatientId")]
        public int Id { get; set; }
       
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Mobile { get; set; }
        [Required]
        public string Email { get; set; }
        public string Address { get; set; }
        [Required]
        public string ZipCode { get; set; }
        [Required]
        public string Age { get; set; }
        [Required]
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Required]
        public int ExternalId { get; set; }
        [Required]
        public string PreferredLanguage { get; set; }
        [ForeignKey("OrganizationId")]
        public int OrganizationId { get; set; }
        public Organization Organization { get; set; }
       
        [ForeignKey("UserId")]
        public int UserId { get; set; }
        public User User { get; set; }
        [Required]
        [Column(TypeName = "bit")]
        public bool IsDeleted { get; set; } = false;
        public string StatusTypeEnum => IsActive.ToString();
        [Column(TypeName = "nvarchar(max)")]
        [Display(Name = "Status")]
        [Required]
        public Status IsActive { get; set; }
        public string GetFullName()
        {
            return $"{FirstName},{LastName}";
        }
        public Patient()
        {
            IsActive = Status.Active;
        }
    }

}
