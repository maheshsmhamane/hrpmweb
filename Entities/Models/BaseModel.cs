﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Entities.Models
{
    public abstract class BaseModel
    {
       
        public string CreatedBy { get; set; }
       
        [DataType(DataType.Date)]
        public DateTime? CreatedOn { get; set; }
        
        public string LastModifiedBy { get; set; }
       
        [DataType(DataType.Date)]
        public DateTime? LastModifiedOn { get; set; }
    }
}
