﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Entities.Models
{
    public class ActivityLog :IEntity
    {

        [Key]
        [Column("LogId")]
        public int Id { get; set; }
        
        public int UserId { get; set; }
        [Required]
        public string WebServerName { get; set; }
        [Required]
        public string IpAddress { get; set; }
        [Required]
        public string ApiType { get; set; }
        [Required]
        public string Api { get; set; }
        public string RequestObject { get; set; }
        [Required]
        public string Response { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        
    }
}


//[LogId] [bigint] IDENTITY(1,1) primary key NOT NULL,

//    [UserId] [bigint] NOT NULL,

//    [WebServerName] [varchar](250) NOT NULL,

//    [IpAddress] [varchar](250) NOT NULL,

//    [ApiType] [varchar](250) NOT NULL,

//    [Api] [varchar](500) NOT NULL,

//    [RequestObject] [varchar](500) NULL,
//	[Response] [varchar](500) NOT NULL,

//    [CreatedBy] [varchar](50) NOT NULL,

//    [CreatedOn] [datetime] NOT NULL