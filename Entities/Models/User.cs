﻿using HRPM.Entities.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Entities.Models
{
	public class User : BaseModel,IEntity
	{

		[Key]
		[Column("UserId")]
		public int Id { get; set; }
		[Required(ErrorMessage = "Please Enter User Name")]
		[Display(Name = "Name")]
		[StringLength(100)]
		[RegularExpression(@"^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$")]
		[DataType(DataType.Text)]
		public string Name { get; set; }

		[Required(ErrorMessage = "Please Enter EmailId")]
		[DataType(DataType.EmailAddress)]
		[RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$")]
		public string Email { get; set; }


		[Required(ErrorMessage = "Please Enter User Mobile NO")]
		[Display(Name = "Mobile Number")]
		[Phone]
		[RegularExpression("^[0-9]*$", ErrorMessage = "Mobile NO Must be Numeric")]
		public string Mobile { get; set; }

		[Required]
		public string Address { get; set; }

		[ForeignKey("RoleId")]
		[Required]
		public int RoleId { get; set; }

		public Role Role { get; set; }
		
		[ForeignKey("OrganizationId")]
		public int? OrganizationId { get; set; }

		public Organization Organization { get; set; }
		[Required]
		[Column(TypeName = "bit")]
		public bool IsDeleted { get; set; } = false;
		public string StatusTypeEnum => IsActive.ToString();
		[Column(TypeName = "nvarchar(max)")]
		[Display(Name = "Status")]
		[Required]
		public Status IsActive { get; set; }

		public User()
		{
			IsActive = Status.Active;
		}

	}
}
