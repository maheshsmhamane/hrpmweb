﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Entities.Models
{
    public class Organization:BaseModel,IEntity
    {
        
        [Key]
        [Column("OrgnizationId")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Please Enter Organization Name")]
        [Display(Name = "Name")]
        [StringLength(100)]
        [RegularExpression(@"^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$")]
        [DataType(DataType.Text)]
        [Column("OrganizationName")]
        public string OrganizationName { get; set; }

        [Required(ErrorMessage = "Please Enter EmailId")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please Enter Organization Mobile NO")]
        [Display(Name = "Mobile Number")]
        [Phone]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Mobile NO Must be Numeric")]
        public string Contact { get; set; }

        [Required(ErrorMessage = "Please Enter Owner Name")]
        [Display(Name = "Owner Name")]
        [StringLength(100)]
        
        [DataType(DataType.Text)]
        public string OwnerName { get; set; }
        [Required(ErrorMessage = "Please Enter Owner Contact")]
        //[Display(Name = "Owner Contact No")]
        [Phone]
        public string OwnerContact { get; set; }
        [Required(ErrorMessage = "Please Enter Address ")]
        [StringLength(300)]
        public string Address { get; set; }
        [Required(ErrorMessage = "Please Enter ZipCode")]
        [Display(Name = "Zip Code")]
        [DataType(DataType.PostalCode)]
        public int ZipCode { get; set; }
        [Required]
        [Column(TypeName = "bit")]
        public bool IsDeleted { get; set; } = false;
    }
}
