﻿using System.Collections.Generic;
using System.Text;

namespace HRPM.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
