﻿using HRPM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HRPM.Entities.Extensions
{
   public static class MappingExtension
    {
        public static void Map(this Device dbDevice, Device device)
        {
            dbDevice.DeviceName = device.DeviceName;
            dbDevice.DeviceType = device.DeviceType;
            dbDevice.IsAssigned = device.IsAssigned;
            dbDevice.IsAuthenticated = device.IsAuthenticated;
            dbDevice.LastModifiedBy = device.LastModifiedBy;
            dbDevice.LastModifiedOn = DateTime.Now; 
        }
        public static void Map(this Patient dbPatients, Patient patients)
        {
            dbPatients.Address = patients.Address;
            dbPatients.Email = patients.Email;
            dbPatients.FirstName = patients.FirstName;
            dbPatients.Gender = patients.Gender;
            dbPatients.IsDeleted = patients.IsDeleted;
            dbPatients.LastModifiedOn = DateTime.Now;
            dbPatients.LastModifiedBy = patients.LastModifiedBy;
            dbPatients.LastName = patients.LastName;
            dbPatients.MaritalStatus = patients.MaritalStatus;
            dbPatients.PreferredLanguage = patients.PreferredLanguage;
            dbPatients.DateOfBirth = patients.DateOfBirth;
            dbPatients.Mobile = patients.Mobile;
            dbPatients.ZipCode = patients.ZipCode;
        }
        public static void Map(this Organization dbOrganization, Organization organization)
        {
            dbOrganization.Address = organization.Address;
            dbOrganization.Email = organization.Email;
            dbOrganization.OrganizationName = organization.OrganizationName;
            dbOrganization.OwnerContact = organization.OwnerContact;
            dbOrganization.OwnerName = organization.OwnerName;
            dbOrganization.LastModifiedOn = DateTime.Now;
            dbOrganization.LastModifiedBy = organization.LastModifiedBy;
            dbOrganization.ZipCode = organization.ZipCode;
        }
        //public static void Map(this PatientHealthReading dbDevice, PatientHealthReading device)
        //{
        //    dbDevice. = device.Device;
        //    dbDevice.Email = device.Email;
        //    dbDevice.Name = device.Name;
        //    dbDevice.OwnerContact = device.OwnerContact;
        //    dbDevice.OwnerName = device.OwnerName;
        //    dbDevice.LastModifiedOn = DateTime.Now;
        //    dbDevice.LastModifiedBy = device.LastModifiedBy;
        //    dbDevice.ZipCode = device.ZipCode;
        //}
        public static void Map(this PatientDeviceAssignment dbPatientDeviceAssignment, PatientDeviceAssignment deviceAssignment)
        {
            dbPatientDeviceAssignment.DeviceId = deviceAssignment.DeviceId;
            dbPatientDeviceAssignment.PatientId = deviceAssignment.PatientId;
            dbPatientDeviceAssignment.UnAssignedDate = deviceAssignment.UnAssignedDate;
            dbPatientDeviceAssignment.LastModifiedOn = DateTime.Now;
            dbPatientDeviceAssignment.LastModifiedBy = deviceAssignment.LastModifiedBy;
            dbPatientDeviceAssignment.CreatedBy = deviceAssignment.CreatedBy;
            //dbDevice.CreatedOn = device.CreatedOn;
        }
        public static void Map(this User dbUser, User user)
        {
            dbUser.Address = user.Address;
            dbUser.OrganizationId = user.OrganizationId;
            dbUser.Email = user.Email;
            dbUser.Name = user.Name;
            dbUser.Mobile = user.Mobile;
            dbUser.LastModifiedOn = DateTime.Now;
            dbUser.RoleId = user.RoleId;
            dbUser.LastModifiedBy = user.LastModifiedBy;
        }
        public static void Map(this Role dbRole, Role role)
        {
            dbRole.RoleType = role.RoleType;
            dbRole.LastModifiedOn = DateTime.Now;
            dbRole.LastModifiedBy = role.LastModifiedBy;
        }
    }
}
