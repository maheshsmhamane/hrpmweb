﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HRPM.Entities.Extensions
{
    public static class IEntityExtensions
    {
        public static bool IsObjectNull(this IEntity entity)
        {
            return entity == null;
        }

        public static bool IsEmptyObject(this IEntity entity)
        {
            int i = 0;
            if (entity!= null)
                return entity.Id.Equals(i);
            else
                return true;
        }
    }
}
