﻿using HRPM.Contracts;
using HRPM.Entities.Extensions;
using HRPM.Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Controllers.WebApi
{
	[Route("api/patients/{patientId}/deviceassignments")]
	[ApiController]
	public class PatientDeviceAssignmentController : ControllerBase
	{
		private ILoggerManager _logger;
		private IRepositoryWrapper _repository;

		public PatientDeviceAssignmentController(ILoggerManager logger, IRepositoryWrapper repository)
		{
			_logger = logger;
			_repository = repository;
		}

		[HttpGet]
		public IActionResult GetPatientDeviceAssignments(int patientId, [FromQuery] PatientDeviceAssignmentParameters patientDeviceAssignmentParameters)

		{

			var patientsDevices = _repository.PatientDeviceAssignment.GetPatientDeviceAssignment(patientId,patientDeviceAssignmentParameters);

			var metadata = new
			{
				patientsDevices.TotalCount,
				patientsDevices.PageSize,
				patientsDevices.CurrentPage,
				patientsDevices.TotalPages,
				patientsDevices.HasNext,
				patientsDevices.HasPrevious
			};

			Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

			_logger.LogInfo($"Returned {patientsDevices.TotalCount} PatientDeviceAssignments from database.");

			return Ok(patientsDevices);
		}


		
		[HttpGet("{id}", Name = "PatientDevicesById")]
		public IActionResult GetPatientDeviceAssignmentsById(int patientId, int DeviceId)
		{
			var patientDevice = _repository.PatientDeviceAssignment.GetPatientDeviceAssignmentById(patientId, DeviceId);

			if (patientDevice.IsEmptyObject())
			{
				_logger.LogError($"PatientDeviceAssignments with id: {DeviceId}, hasn't been found in db.");
				return NotFound();
			}
			else
			{
				_logger.LogInfo($"Returned PatientDeviceAssignments with id: {DeviceId}");
				return Ok(patientDevice);
			}
		}

		[HttpPost]
		public IActionResult CreatePatientDeviceAssignments(int patientId,[FromBody] PatientDeviceAssignment patientDeviceAssignment)
		{
			patientDeviceAssignment.PatientId = patientId;
			if (patientDeviceAssignment.IsObjectNull())
			{
				_logger.LogError("PatientDeviceAssignments object sent from client is null.");
				return BadRequest("PatientDeviceAssignments object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid PatientDeviceAssignments object sent from client.");
				return BadRequest("Invalid PatientDeviceAssignments object");
			}

			_repository.PatientDeviceAssignment.CreatePatientDeviceAssignment(patientDeviceAssignment);
			_repository.Save();

			return CreatedAtRoute("PatientDevicesById", new { id = patientDeviceAssignment.Id ,patientId =patientDeviceAssignment.PatientId}, patientDeviceAssignment);
		}

		[HttpPut("{id}")]
		public IActionResult UpdatePatientDeviceAssignmnets(int patientId, int id, [FromBody] PatientDeviceAssignment patientDeviceAssignment)
		{
			if (patientDeviceAssignment.IsObjectNull())
			{
				_logger.LogError("PatientDeviceAssignmnets object sent from client is null.");
				return BadRequest("PatientDeviceAssignmnets object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid PatientDeviceAssignmnets object sent from client.");
				return BadRequest("Invalid PatientDeviceAssignmnets object");
			}

			var dbOwner = _repository.PatientDeviceAssignment.GetPatientDeviceAssignmentById(patientId, id);
			if (dbOwner.IsEmptyObject())
			{
				_logger.LogError($"PatientDeviceAssignmnets with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.PatientDeviceAssignment.UpdatePatientDeviceAssignment(dbOwner, patientDeviceAssignment);
			_repository.Save();

			return NoContent();
		}

		[HttpDelete("{id}")]
		public IActionResult DeletePatientDeviceAssignmnets(int patientId, int id)
		{
			var device = _repository.PatientDeviceAssignment.GetPatientDeviceAssignmentById(patientId,id);
			if (device.IsEmptyObject())
			{
				_logger.LogError($"PatientDeviceAssignmnets with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.PatientDeviceAssignment.DeletePatientDeviceAssignment(device);
			_repository.Save();

			return NoContent();
		}


	}
	
}
