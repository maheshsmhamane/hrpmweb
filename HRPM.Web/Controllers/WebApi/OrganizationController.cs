﻿using HRPM.Contracts;
using HRPM.Entities.Extensions;
using HRPM.Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Controllers.WebApi
{
    

	[Route("api/organizations")]
	[ApiController]
	public class OrganizationController : ControllerBase
	{
		private ILoggerManager _logger;
		private IRepositoryWrapper _repository;

		public OrganizationController(ILoggerManager logger, IRepositoryWrapper repository)
		{
			_logger = logger;
			_repository = repository;
		}

		[HttpGet]
		public IActionResult GetOrzanization([FromQuery] OrganizationParameters organizationParameters)
		{
			var organizations = _repository.Organization.GetOrganization(organizationParameters);

			var metadata = new
			{
				organizations.TotalCount,
				organizations.PageSize,
				organizations.CurrentPage,
				organizations.TotalPages,
				organizations.HasNext,
				organizations.HasPrevious,
				organizations

			};
			
			//Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

			_logger.LogInfo($"Returned {organizations.TotalCount} Organization from database.");

			
			return Ok(metadata);

			
		}   

		[HttpGet("{id}", Name = "OrganizationById")]
		public IActionResult GetOrganizationById(int id)
		{
			var organization = _repository.Organization.GetOrganizationById(id);

			if (organization.IsEmptyObject())
			{
				_logger.LogError($"Organization with id: {id}, hasn't been found in db.");
				return NotFound();
			}
			else
			{
				_logger.LogInfo($"Returned Organization with id: {id}");
				return Ok(organization);
			}
		}

		[HttpPost]
		public IActionResult CreateOrganization([FromBody] Organization organization)
		{

			if (organization.IsObjectNull())
			{
				_logger.LogError("Organization object sent from client is null.");
				return BadRequest("Organization object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid Organization object sent from client.");
				return BadRequest("Invalid Organization object");
			}
			organization.CreatedOn = DateTime.Now;
			organization.CreatedBy = "Mahesh";
			
			_repository.Organization.CreateOrganization(organization);
			_repository.Save();

			//return CreatedAtRoute("OrganizationById", new { id = organization.Id }, organization);
			return Ok();
		}

		[HttpPut("{id}")]
		public IActionResult UpdateOrganization(int id, [FromBody] Organization organization)
		{
			if (organization.IsObjectNull())
			{
				_logger.LogError("Organization object sent from client is null.");
				return BadRequest("Organization object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid Organization object sent from client.");
				return BadRequest("Invalid Organization object");
			}

			var dbOwner = _repository.Organization.GetOrganizationById(id);
			if (dbOwner.IsEmptyObject())
			{
				_logger.LogError($"Organization with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.Organization.UpdateOrganization(dbOwner, organization);
			_repository.Save();

			return Ok();
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteOrganization(int id)
		{
			var organization = _repository.Organization.GetOrganizationById(id);
			if (organization.IsEmptyObject())
			{
				_logger.LogError($"Organization with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.Organization.DeleteOrganization(organization);
			_repository.Save();

			return NoContent();
		}

	}

}
