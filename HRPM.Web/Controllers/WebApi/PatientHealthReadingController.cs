﻿using HRPM.Contracts;
using HRPM.Entities.Extensions;
using HRPM.Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Controllers.WebApi
{
	[Route("api/devices/{deviceId}/healthreadings")]
	[ApiController]
	public class PatientHealthReadingController : ControllerBase
	{
		private ILoggerManager _logger;
		private IRepositoryWrapper _repository;

		public PatientHealthReadingController(ILoggerManager logger, IRepositoryWrapper repository)
		{
			_logger = logger;
			_repository = repository;
		}

		[HttpGet]
		public IActionResult GetPatientHealthReading(int deviceId, [FromQuery] PagingParameters pagingParameters)

		{
			var healthReadings = _repository.PatientHealthReading.GetPatientHealthReading(deviceId, pagingParameters);

			var metadata = new
			{
				healthReadings.TotalCount,
				healthReadings.PageSize,
				healthReadings.CurrentPage,
				healthReadings.TotalPages,
				healthReadings.HasNext,
				healthReadings.HasPrevious
			};

			Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

			_logger.LogInfo($"Returned {healthReadings.TotalCount} int PatientHealthReading from database.");

			return Ok(healthReadings);
		}


		
		[HttpGet("{id}", Name ="ReadingsById")]
		public IActionResult GetPatientHealthReadingById(int deviceId, int id)
		{
			var patientHealthReading = _repository.PatientHealthReading.GetPatientHealthReadingById(deviceId, id);

			if (patientHealthReading.IsEmptyObject())
			{
				_logger.LogError($"PatientHealthReading with id: {id}, hasn't been found in db.");
				return NotFound();
			}
			else
			{
				_logger.LogInfo($"Returned PatientHealthReading with id: {id}");
				return Ok(patientHealthReading);
			}
		}

		[HttpPost]
		public IActionResult CreatePatientHealthReading(int deviceId,[FromBody] PatientHealthReading patientHealthReading)
		{
			patientHealthReading.DeviceId = deviceId;
			if (patientHealthReading.IsObjectNull())
			{
				_logger.LogError("Device object sent from client is null.");
				return BadRequest("Device object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid Device object sent from client.");
				return BadRequest("Invalid Device object");
			}

			_repository.PatientHealthReading.CreatePatientHealthReading(patientHealthReading);
			_repository.Save();

			return CreatedAtRoute("ReadingsById", new { id = patientHealthReading.Id ,deviceId=patientHealthReading.DeviceId }, patientHealthReading);
		}

		[HttpPut("{id}")]
		public IActionResult UpdatePatientHealthReading(int deviceId, int id, [FromBody] PatientHealthReading patientHealthReading)
		{
			if (patientHealthReading.IsObjectNull())
			{
				_logger.LogError("PatientHealthReading object sent from client is null.");
				return BadRequest("PatientHealthReading object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid PatientHealthReading object sent from client.");
				return BadRequest("Invalid PatientHealthReading object");
			}
				var dbOwner = _repository.PatientHealthReading.GetPatientHealthReadingById(deviceId,id);
			if (dbOwner.IsEmptyObject())
			{
				_logger.LogError($"Device with id: {id}, hasn't been found in db.");
				return NotFound();
				
			}
			_repository.PatientHealthReading.UpdatePatientHealthReading(dbOwner, patientHealthReading);
			_repository.Save();

			return NoContent();
		}

		[HttpDelete("{id}")]
		public IActionResult DeletePatientHealthReading(int deviceId, int id)
		{
			var device = _repository.PatientHealthReading.GetPatientHealthReadingById(deviceId,id);
			if (device.IsEmptyObject())
			{
				_logger.LogError($"PatientHealthReading with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.PatientHealthReading.DeletePatientHealthReading(device);
			_repository.Save();

			return NoContent();
		}


	}
	
}
