﻿using HRPM.Contracts;
using HRPM.Entities.Extensions;
using HRPM.Entities.Models;
using Microsoft.AspNetCore.Mvc;
using System;

namespace HRPM.Controllers.WebApi
{
    [Route("api/organizations")]
	[ApiController]
	public class UserController : ControllerBase
	{
		private ILoggerManager _logger;
		private IRepositoryWrapper _repository;

		public UserController(ILoggerManager logger, IRepositoryWrapper repository)
		{
			_logger = logger;
			_repository = repository;
		}
		
		[HttpGet("{organizationId}/users")]   //get users by organization id from specific organization
		public IActionResult GetUsers(int organizationId, [FromQuery] UserParameters userParameters)

		{
			var users = _repository.User.GetUser(organizationId,userParameters);

			var metadata = new
			{
				users.TotalCount,
				users.PageSize,
				users.CurrentPage,
				users.TotalPages,
				users.HasNext,
				users.HasPrevious,
				users

			};

			//Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

			_logger.LogInfo($"Returned {users.TotalCount} uers from database.");

			return Ok(metadata);
		}

		[HttpGet("allusers")]  //get users All users from all organization  
		public IActionResult GetAllUsers([FromQuery] UserParameters userParameters)

		{
			var users = _repository.User.GetUser(userParameters);

			var metadata = new
			{
				users.TotalCount,
				users.PageSize,
				users.CurrentPage,
				users.TotalPages,
				users.HasNext,
				users.HasPrevious,
				users

			};

			//Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

			_logger.LogInfo($"Returned {users.TotalCount} uers from database.");

			return Ok(metadata);
		}

		[HttpGet("users/{id}", Name = "UserById")]
		public IActionResult GetUserById( int id)
		{
			var users = _repository.User .GetUserById(id);

			if (users.IsEmptyObject())
			{
				_logger.LogError($"Users with id: {id}, hasn't been found in db.");
				return NotFound();
			}
			else
			{
				_logger.LogInfo($"Returned Users with id: {id}");
				return Ok(users);
			}
		}

        [HttpPost("users")]
		public IActionResult CreateUsers([FromBody] User user)
		{
			if (user.IsObjectNull())
			{
				_logger.LogError("Users object sent from client is null.");
				return BadRequest("Users object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid Users object sent from client.");
				return BadRequest("Invalid Users object");
			}
			user.CreatedOn = DateTime.Now;
			user.CreatedBy = "Mahesh";

			_repository.User.CreateUser(user);
			_repository.Save();

			return Ok(); 
				//CreatedAtRoute("UserById", new { id = user.Id, organizationId = user.OrganizationId }, user);
		}

		[HttpPut("users/{id}")]
		public IActionResult UpdateUser( int id, [FromBody] User user)
		{
			if (user.IsObjectNull())
			{
				_logger.LogError("Users object sent from client is null.");
				return BadRequest("Users object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid Users object sent from client.");
				return BadRequest("Invalid Users object");
			}

			var dbOwner = _repository.User.GetUserById(id);
			if (dbOwner.IsEmptyObject())
			{
				_logger.LogError($"Users with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.User.UpdateUser(dbOwner, user);
			_repository.Save();

			return Ok();
		}

		[HttpDelete("users/{id}")]
		public IActionResult DeleteUser(int id)
		{
			var user = _repository.User.GetUserById(id);
			if (user.IsEmptyObject())
			{
				_logger.LogError($"Users with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.User.DeleteUser(user);
			_repository.Save();

			return NoContent();
		}
	}
	
}
