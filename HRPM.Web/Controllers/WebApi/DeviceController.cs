﻿using HRPM.Contracts;
using HRPM.Entities.Extensions;
using HRPM.Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Controllers.WebApi
{
	[Route("api/devices")]
	[ApiController]
	public class DeviceController : ControllerBase
	{
		private ILoggerManager _logger;
		private IRepositoryWrapper _repository;

		public DeviceController(ILoggerManager logger, IRepositoryWrapper repository)
		{
			_logger = logger;
			_repository = repository;
		}

		[HttpGet]
		public IActionResult GetDevices([FromQuery] DeviceParameters deviceParameters)

		{
			var device = _repository.Device.GetDevice(deviceParameters);

			var metadata = new
			{
				device.TotalCount,
				device.PageSize,
				device.CurrentPage,
				device.TotalPages,
				device.HasNext,
				device.HasPrevious
			};

			Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

			_logger.LogInfo($"Returned {device.TotalCount} Devices from database.");

			return Ok(device);
		}


		
		[HttpGet("{id}", Name ="DeviceById")]
		public IActionResult GetDeviceById(int id)
		{
			var device = _repository.Device.GetDeviceById(id);

			if (device.IsEmptyObject())
			{
				_logger.LogError($"Device with id: {id}, hasn't been found in db.");
				return NotFound();
			}
			else
			{
				_logger.LogInfo($"Returned Device with id: {id}");
				return Ok(device);
			}
		}

		[HttpPost]
		public IActionResult CreateDevice([FromBody] Device device)
		{
			if (device.IsObjectNull())
			{
				_logger.LogError("Device object sent from client is null.");
				return BadRequest("Device object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid Device object sent from client.");
				return BadRequest("Invalid Device object");
			}

			_repository.Device.CreateDevice(device);
			_repository.Save();

			return CreatedAtRoute("DeviceById", new { id = device.Id }, device);
		}

		[HttpPut("{id}")]
		public IActionResult UpdateDevice(int id, [FromBody] Device device)
		{
			if (device.IsObjectNull())
			{
				_logger.LogError("Device object sent from client is null.");
				return BadRequest("Device object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid Device object sent from client.");
				return BadRequest("Invalid Device object");
			}

			var dbOwner = _repository.Device.GetDeviceById(id);
			if (dbOwner.IsEmptyObject())
			{
				_logger.LogError($"Device with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.Device.UpdateDevice(dbOwner, device);
			_repository.Save();

			return NoContent();
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteDevice(int id)
		{
			var device = _repository.Device.GetDeviceById(id);
			if (device.IsEmptyObject())
			{
				_logger.LogError($"Device with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.Device.DeleteDevice(device);
			_repository.Save();

			return NoContent();
		}


	}
	
}
