﻿using HRPM.Contracts;
using HRPM.Entities.Extensions;
using HRPM.Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Controllers.WebApi
{
	[Route("api/organizations/")]
	[ApiController]
	public class PatientController : ControllerBase
	{
		private ILoggerManager _logger;
		private IRepositoryWrapper _repository;

		public PatientController(ILoggerManager logger, IRepositoryWrapper repository)
		{
			_logger = logger;
			_repository = repository;
		}

		[HttpGet("patients")] //All Patients from All organizations
		public IActionResult GetPatient([FromQuery] PatientParameters patientParameters)

		{
			var patients = _repository.Patient.GetPatient(patientParameters);

			var metadata = new
			{
				patients.TotalCount,
				patients.PageSize,
				patients.CurrentPage,
				patients.TotalPages,
				patients.HasNext,
				patients.HasPrevious,
				patients
			};

			Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

			_logger.LogInfo($"Returned {patients.TotalCount} Patient from database.");

			return Ok(metadata);
		}

		[HttpGet("{organizationId}/patients")] //Patinets From Specific Organization
		public IActionResult GetPatient(int organizationId, [FromQuery] PatientParameters patientParameters)
		{
			var patients = _repository.Patient.GetPatient(organizationId, patientParameters);

			var metadata = new
			{
				patients.TotalCount,
				patients.PageSize,
				patients.CurrentPage,
				patients.TotalPages,
				patients.HasNext,
				patients.HasPrevious,
				patients
			};

			Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

			_logger.LogInfo($"Returned {patients.TotalCount} Patient from database.");

			return Ok(metadata);
		}

		[HttpGet("{id}", Name = "PatientById")]
		public IActionResult GetPatientById(int id)
		{
			var patients = _repository.Patient.GetPatientById(id);

			if (patients.IsEmptyObject())
			{
				_logger.LogError($"Patient with id: {id}, hasn't been found in db.");
				return NotFound();
			}
			else
			{
				_logger.LogInfo($"Returned Patient with id: {id}");
				return Ok(patients);
			}
		}

		[HttpPost("{organizationId}/patients")]
		public IActionResult CreatePatient(int organizationId,[FromBody] Patient patient)
		{
			patient.OrganizationId = organizationId;
			if (patient.IsObjectNull())
			{
				_logger.LogError("patient object sent from client is null.");
				return BadRequest("patient object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid patient object sent from client.");
				return BadRequest("Invalid patient object");
			}
			patient.CreatedBy = "Mahesh";
			patient.CreatedOn = DateTime.Now; 
			_repository.Patient.CreatePatient(patient);
			_repository.Save();

			return CreatedAtRoute("PatientById", new { id = patient.Id }, patient);
		}

		[HttpPut("{organizationId}/patients/{id}")]
		public IActionResult UpdatePatient(int id, [FromBody] Patient patient)
		{
			if (patient.IsObjectNull())
			{
				_logger.LogError("patient object sent from client is null.");
				return BadRequest("patient object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid patient object sent from client.");
				return BadRequest("Invalid patient object");
			}

			var dbOwner = _repository.Patient.GetPatientById(id);
			if (dbOwner.IsEmptyObject())
			{
				_logger.LogError($"Organization with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.Patient.UpdatePatient(dbOwner, patient);
			_repository.Save();

			return NoContent();
		}

		[HttpDelete("{organizationId}/patients/{id}")]
		public IActionResult DeletePatient(int id)
		{
			var patients = _repository.Patient.GetPatientById(id);
			if (patients.IsEmptyObject())
			{
				_logger.LogError($"Patient with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.Patient.DeletePatient(patients);
			_repository.Save();

			return NoContent();
		}


	}

}
