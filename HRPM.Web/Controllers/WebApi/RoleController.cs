﻿using HRPM.Contracts;
using HRPM.Entities.Extensions;
using HRPM.Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Controllers.WebApi
{
	[Route("api/roles")]
	[ApiController]
	public class RoleController : ControllerBase
	{
		private ILoggerManager _logger;
		private IRepositoryWrapper _repository;

		public RoleController(ILoggerManager logger, IRepositoryWrapper repository)
		{
			_logger = logger;
			_repository = repository;
		}

		[HttpGet]
		public IActionResult GetRoles([FromQuery] PagingParameters pagingParameters)

		{
			var roles = _repository.Role.GetRole(pagingParameters);

			var metadata = new
			{
				roles
			};

			//Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

			_logger.LogInfo($"Returned {roles.TotalCount} Roles from database.");

			return Ok(metadata);
		}



		[HttpGet("{id}", Name = "RoleById")]
		public IActionResult GetRoleById(int id)
		{
			var roles = _repository.Role.GetRoleById( id);

			if (roles.IsEmptyObject())
			{
				_logger.LogError($"Roles with id: {id}, hasn't been found in db.");
				return NotFound();
			}
			else
			{
				_logger.LogInfo($"Returned Roles with id: {id}");
				return Ok(roles);
			}
		}

		[HttpPost]
		public IActionResult CreateRoles([FromBody] Role role)
		{
			
			if (role.IsObjectNull())
			{
				_logger.LogError("Roles object sent from client is null.");
				return BadRequest("Roles object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid Roles object sent from client.");
				return BadRequest("Invalid Roles object");
			}

			_repository.Role.CreateRole(role);
			_repository.Save();

			return CreatedAtRoute("RoleById", new { id = role.Id }, role);
		}

		[HttpPut("{id}")]
		public IActionResult UpdateRole(int id, [FromBody] Role role)
		{
			if (role.IsObjectNull())
			{
				_logger.LogError("Roles object sent from client is null.");
				return BadRequest("Roles object is null");
			}

			if (!ModelState.IsValid)
			{
				_logger.LogError("Invalid Roles object sent from client.");
				return BadRequest("Invalid Roles object");
			}

			var dbOwner = _repository.Role.GetRoleById(id);
			if (dbOwner.IsEmptyObject())
			{
				_logger.LogError($"Roles with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.Role.UpdateRole(dbOwner, role);
			_repository.Save();

			return NoContent();
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteRole( int id)
		{
			var role = _repository.Role.GetRoleById(id);
			if (role.IsEmptyObject())
			{
				_logger.LogError($"Roles with id: {id}, hasn't been found in db.");
				return NotFound();
			}

			_repository.Role.DeleteRole(role);
			_repository.Save();

			return NoContent();
		}


	}

}
