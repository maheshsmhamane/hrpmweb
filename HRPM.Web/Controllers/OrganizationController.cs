﻿using HRPM.Contracts;
using HRPM.Entities.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HRPM.Controllers
{
    public class OrganizationController : Controller
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        public OrganizationController(ILoggerManager logger, IRepositoryWrapper repository)
        {
            _logger = logger;
            _repository = repository;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult OrganizationDetails(int id)
        {
            var organization = _repository.Organization.GetOrganizationById(id);

            if (organization.IsEmptyObject())
            {
                _logger.LogError($"Organization with id: {id}, hasn't been found in db.");
                return NotFound();
            }
            else
            {
                _logger.LogInfo($"Returned Organization with id: {id}");
                return View(organization);
            }
        }

    }
}
