﻿"use strict";

let userIdToDelete = 0;
let userIdToEdit = 0;

function saveUser() {
    if ($("#user-name").val().trim().length === 0) {
        toastr.error("Please enter  Name.");
        return;
    }

    if ($("#user-email").val().trim().length === 0) {
        toastr.error("Please enter email.");
        return;
    }

    if ($("#user-contact").val().trim().length === 0) {
        toastr.error("Please enter Contact.");
        return;
    }
    if (($("#user-role").val()==='0')) {
        toastr.error("Please Select  Role.");
        return;
    }
    if ($("#user-address").val().trim().length === 0) {
        toastr.error("Please enter Address.");
        return;
    }

    let userRequest = {
        "name": $("#user-name").val(),
        "email": $("#user-email").val(),
        "mobile": $("#user-contact").val(),
        "roleId": parseInt($("#user-role").val()),
        "address": $("#user-address").val(),
        "organizationId":$("#user-organization").val()
    }
    
    if (userIdToEdit > 0) {
        httpHelper.Put(API.User_API + "users/" + userIdToEdit, JSON.stringify(userRequest), (data) => {
            toastr.info("User was updated successfully.");
            $("#add-user-modal").modal('hide');
            getUsers();
        });

        userIdToEdit = 0;
        return;
    }

    httpHelper.Post(API.User_API + "users/", JSON.stringify(userRequest), (data) => {
        toastr.info("User was saved successfully.");
        $("#add-user-modal").modal('hide');
        getUsers();
    });
}

function getUsers(pagedata) {
    
    var PageSize = $("#pages").val();
    var Status = '';
    Status = $("#status").val();
    var Organization = $("#selectedOrganization").val();
    var hasNext = pagedata;
    var PageNumber = 1;
    var n = parseInt(localStorage.getItem("currentPage"));
    if (hasNext == 'next') {
        var t = parseInt(localStorage.getItem("totalPages"));
        if (n)
            PageNumber = n + 1;
        if (t)
            if (PageNumber > t) { PageNumber = 1; }
    }
    else if (hasNext == 'previous') {
        if (n)
            PageNumber = n - 1;
        if (PageNumber <= 0) { PageNumber = 1; }
    }


    httpHelper.Get(API.User_API + 'allusers/' + '?PageSize=' + PageSize + '&PageNumber=' + PageNumber + '&Status=' + Status + '&Organization=' + Organization, (data) => {
        let usersdata = JSON.parse(data);
        let users = usersdata.users;
        if (!users) { return; }
        let totalCount = usersdata.totalCount;
        let totalPages = usersdata.totalPages;
        let currentPage = usersdata.currentPage;
        localStorage.removeItem("currentPage");
        localStorage.removeItem("totalPages");
        localStorage.setItem("currentPage", currentPage);
        localStorage.setItem("totalPages", totalPages);

        let nextSize = (PageSize * currentPage);
        let previousSize = (nextSize - PageSize) + 1;
        let pageNavigation = previousSize + " to " + nextSize + " of " + totalPages;

        $("#has-next").text(usersdata.hasNext);
        $("#has-previous").text(usersdata.hasPrevious);
        $("#total-count").text(totalCount);
        $("#page-size").text(PageSize);
        $("#total-pages").text(totalPages);
        $("#page-navigation").text(pageNavigation);


        $("#user-list tbody").empty();
        if (users.length === 0) {
            $(".msg-no-records").show();
            return;
        }
        $(".msg-no-records").hide();
        if (users.length > 0) {
            $("#user-table").show();
        }
        else {
            $("#user-table").hide();
            $(".msg-no-records").show();
            return;
        }
        
        users.forEach(usr => {
            let row = "<tr>";
            row += "<td >" + "<a href=/user/userdetails/" + usr.userId + ">" + usr.name + "</a>" + "</td >";
            row += "<td>" + (usr.email ? usr.email : "") + "</td>";
            row += "<td>" + (usr.phone ? usr.phone : "") + "</td>";
            row += "<td>" + (usr.role ? usr.role : "") + "</td>";
            row += "<td>" + (usr.organizationName ? usr.organizationName : "") + "</td>";
            if (usr.status == 'Active') { row += "<td class='statusActive'>" + (usr.status ? usr.status : "") + "</td>"; }
            if (usr.status == 'InActive') { row += "<td class='statusInActive'>" + (usr.status ? usr.status : "") + "</td>"; }
            row += "<td class='actions'>";
            row += "<span class='glyphicon glyphicon-pencil' onclick='setUserToEdit(" + usr.userId +
                "," + usr.organizationId + "," + "\"" + usr.name + "\"" +
                ","  + "\"" + usr.email + "\"" +
                "," + "\"" + usr.phone + "\"" +
                "," + "\"" + usr.roleId + "\"" +
                "," + "\"" + usr.address + "\"" +
                ")'></span> ";
            row += "<span class='glyphicon glyphicon-trash' data-toggle='modal' data-target='#delete-user-modal' onclick='setUserToDelete(" + usr.userId + ")'></span>";
            row += "</td>";
            row += "</tr>";
            $("#user-list tbody").append(row);
        });

    }, () => {
            
    });

}

function deleteUser() {
    httpHelper.Delete(API.User_API + "users/" + userIdToDelete, (data) => {
        toastr.info("User was deleted successfully.");
        userIdToDelete = 0;
        getUsers();
    }, () => {

    });
}

function setUserToDelete(id) {
    userIdToDelete = id;
}

function setUserToEdit(id, organizationId, name, email, phone, roleId, address) {
    userIdToEdit = id;
    $("#add-user-modal").modal();
    $("#user-name").val(name),
    $("#user-email").val(email),
    $("#user-contact").val(phone),
    $('#user-role').val(roleId);
    $('#user-organization').val(organizationId);
    $("#user-address").val(address);
    $("#save-edit-button").html("Update User");
}

function openAddUserModal() {
   // getRoleDropdown();
    $("#add-user-modal").modal();
    $("#user-name").val(""),
        $("#user-email").val(""),
        $("#user-contact").val(""),
        $("#user-address").val(""),
        $("#save-edit-button").html("Add User");
       
    //document.getElementById("user-is-non-admin").checked = true;
}

function getOrganizationsDropdown() {
    httpHelper.Get(API.Organization_API , (data) => {
        let usersdata = JSON.parse(data);
        let organizations = usersdata.organizations;
        organizations.forEach(org => {
            $('#selectedOrganization').append('<option value="' + org.name + '">' + org.name + '</option>');
            $('#user-organization').append('<option value="' + org.id + '">' + org.name + '</option>');
        });
    }, () => {

    });
}

function getRoleDropdown() {
    httpHelper.Get(API.Role_API, (data) => {
        let usersdata = JSON.parse(data);
        let roles = usersdata.roles;
        roles.forEach(org => {
            $('#user-role').append('<option value="' + org.id + '">' + org.roleType + '</option>');
        });
    }, () => {

   });
}
