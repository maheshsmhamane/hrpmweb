﻿"use strict";

let patientIdToDelete = 0;
let patientIdToEdit = 0;


function savePatient() {
    if ($("#patient-name").val().trim().length === 0) {
        toastr.error("Please enter  Name.");
        return;
    }


    if ($("#patient-email").val().trim().length === 0) {
        toastr.error("Please enter email.");
        return;
    }
    //if (!validateEmail($("#patient-email").val()))  {
    //    toastr.error("Please enter Valid email.");
    //    return; }

    if ($("#patient-contact").val().trim().length === 0) {
        toastr.error("Please enter Contact.");
        return;
    }

    if ($("#patient-ownerName").val().trim().length === 0) {
        toastr.error("Please enter OwnerName .");
        return;
    }

    if ($("#patient-ownerContact").val().trim().length === 0) {
        toastr.error("Please enter OwnerContactNo.");
        return;
    }

    if ($("#patient-address").val().trim().length === 0) {
        toastr.error("Please enter Address.");
        return;
    }
    if ($("#patient-zipCode").val().trim().length === 0) {
        toastr.error("Please enter ZipCode.");
        return;
    }

    let userRequest = {
        "userName": $("#patient-name").val(),
        "email": $("#patient-email").val(),
        "contact": $("#patient-contact").val(),
        "ownerName": $("#patient-ownerName").val(),
        "ownerContact": $("#patient-ownerContact").val(),
        "address": $("#patient-address").val(),
        "zipCode": $('#patient-zipCode').val()
    }

    if (userIdToEdit > 0) {
        httpHelper.Put(API.User_API + "/" + userIdToEdit, JSON.stringify(userRequest), (data) => {
            toastr.info("patient was updated successfully.");
            $("#add-patient-modal").modal('hide');
            getUsers();
        });

        userIdToEdit = 0;
        return;
    }


    httpHelper.Post(API.User_API, JSON.stringify(userRequest), (data) => {
        console.log("mydata", data);
        toastr.info("patient was saved successfully.");
        $("#add-patient-modal").modal('hide');
        getUsers();
    });
}



function getPatients(organizationId, pagedata) {
    var PageSize = $("#pages").val()
    var hasNext = pagedata;
    var PageNumber = 1;
    var n = parseInt(localStorage.getItem("currentPage"));
    if (hasNext == 'next') {
        var t = parseInt(localStorage.getItem("totalPages"));
        if (n)
            PageNumber = n + 1;
        if (t)
            if (PageNumber > t) { PageNumber = 1; }
    }
    else if (hasNext == 'previous') {
        if (n)
            PageNumber = n - 1;
        if (PageNumber <= 0) { PageNumber = 1; }
    }


    httpHelper.Get(API.User_API + organizationId + '/patients/' + '?PageSize=' + PageSize + '&PageNumber=' + PageNumber, (data) => {
        let usersdata = JSON.parse(data);

        let patients = usersdata.patients;
        if (!patients) { return; }
        let totalCount = usersdata.totalCount;
        let totalPages = usersdata.totalPages;
        let currentPage = usersdata.currentPage;
        localStorage.removeItem("currentPage");
        localStorage.removeItem("totalPages");
        localStorage.setItem("currentPage", currentPage);
        localStorage.setItem("totalPages", totalPages);

        let nextSize = (PageSize * currentPage);
        let previousSize = (nextSize - PageSize) + 1;
        let pageNavigation = previousSize + " to " + nextSize + " of " + totalPages;

        $("#has-next").text(usersdata.hasNext);
        $("#has-previous").text(usersdata.hasPrevious);
        $("#total-count").text(totalCount);
        $("#page-size").text(PageSize);
        $("#total-pages").text(totalPages);
        $("#page-navigation").text(pageNavigation);


        $("#patient-list tbody").empty();
        if (patients.length === 0) {
            $(".msg-no-records").show();
            return;
        }
        $(".msg-no-records").hide();
        if (patients.length > 0) {
            $("#patient-table").show();
        }

        patients.forEach(usr => {
            let row = "<tr>";
            row += "<td >" +/* (usr.name ? usr.name : "") */ "<a href=/patient/patientdetails/" + usr.patientId + ">" + usr.name + "</a>" + "</td >";// "<a href='/UserDetails/" + usr.id + ">"+usr.name+"</a >" 
            row += "<td>" + (usr.address ? usr.address : "") + "</td>";
            row += "<td>" + (usr.gender ? usr.gender : "") + "</td>";
            row += "<td>" + (usr.organizationName ? usr.organizationName : "") + "</td>";
            row += "<td>" + (usr.user ? usr.user : "") + "</td>";
            row += "<td>" + (usr.status ? usr.status : "") + "</td>";
            //row += "<td class='actions'>";
            //row += "<span class='glyphicon glyphicon-pencil' onclick='setUserToEdit(" + usr.userId +
            //    ",\"" + usr.name +
            //    "\"," + "\"" + usr.address + "\"" +
            //    "," + "\"" + usr.email + "\"" +
            //    "," + "\"" + usr.phone + "\"" +
            //    "," + "\"" + usr.role + "\"" +
            //    "," + "\"" + usr.ownerContact + "\"" +
            //    "," + "\"" + usr.ownerName + "\"" +
            //    ")'></span> ";
            //row += "<span class='glyphicon glyphicon-trash' data-toggle='modal' data-target='#delete-patient-modal' onclick='setUserToDelete(" + usr.id + ")'></span>";
            //row += "</td>";
            row += "</tr>";
            $("#patient-list tbody").append(row);
        });

    }, () => {

    });

}



function deletePatient() {
    httpHelper.Delete(API.User_API + "/" + userIdToDelete, (data) => {
        toastr.info("patient was deleted successfully.");
        getUsers();
    }, () => {

    });
}

function setPatientToDelete(id) {
    userIdToDelete = id;
}

function setPatientToEdit(id, name, address, email, phone, zipCode, ownerContact, ownerName) {
    userIdToEdit = id;
    $("#add-patient-modal").modal();
    $("#patient-name").val(name),
        $("#patient-email").val(email),
        $("#patient-contact").val(phone),
        $("#patient-ownerName").val(ownerName),
        $("#patient-ownerContact").val(ownerContact),
        $("#patient-address").val(address),
        $('#patient-zipCode').val(zipCode)
}

function openAddPatientModal() {
    $("#add-patient-modal").modal();
    $("#patient-name").val(""),
        $("#patient-email").val(""),
        $("#patient-contact").val(""),
        $("#patient-ownerName").val(""),
        $("#patient-ownerContact").val(""),
        $("#patient-address").val(""),
        $('#patient-zipCode').val("")
    //document.getElementById("patient-is-non-admin").checked = true;
}
