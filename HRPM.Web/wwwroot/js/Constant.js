﻿class Constants {
    static API_TOKEN = 'vFX-API-token';
    static INSTANCE = 'vFX-instance';
    static SHOUTOUT_SPONSOR_PAGE_SIZE = 6;
    static NO_CHANGES_MADE_MESSAGE = "No changes were made.";
    static IS_LIVE_STREAMING_INITIATOR = 'vFX-is-live-streaming-initiator';
}

class API {
    static SETTINGS_API = '/api/settings';
    static GLOBAL_SETTINGS_API = '/api/settings/global';
    static SPONSORS_API = '/api/files/images/sponsors';
    static SPONSORS_DELETE_API = '/api/files/images/sponsor';
    static SHOUTOUT_VIDEOS_API = '/api/shoutouts';
    static SHOUTOUT_VIDEOS_QUEUE_API = '/api/shoutouts/queue';
    static SHOUTOUT_VIDEOS_QUEUE_STATUS_API = '/api/shoutouts/queue/status';
    static SHOUTOUT_UPLOAD_VIDEO_API = '/api/patrons/shoutouts';
    static USER_AUTHENTICATE_API = '/api/accounts/users/authentication';
    static SHOUTOUT_VIDEO_SPONSORS_API = '/api/shoutouts/sponsors/queue';
    static AUDIO_SETTINGS_API = '/api/settings/audio';
    static AUDIO_FILES_API = '/api/files/audio';
    static EVENT_ATTENDANCE_API = '/api/events/attendance';
    static EVENTS_API = '/api/events';
    static CURRENT_EVENT_API = '/api/teams/events/current';
    static TEAMS_API = '/api/teams';
    static OPPONENTS_API = '/api/teams/opponents';
    static TEAM_EVENTS_API = '/api/teams/events';
    static Organization_API = '/api/organizations';
    static User_API = '/api/organizations/';
    static Role_API = '/api/roles/';
    static VPASSES_API = '/api/teams/vpasses';
    static PATRONS_API = '/api/patrons';
    static PATRONS_SUSPENSION_REVIEW_API = '/api/patrons/suspensions/review';
    static FIRST_ACCEPTED_SHOUTOUT_VIDEO_API = '/api/shoutouts/accepted/first';
    static LIVE_STREAMING_API = '/api/streaming/live';
}

class EventStatus {
    static NOT_STARTED = 'Not Started';
    static STARTED = 'Started';
    static PAUSED = 'Paused';
    static STOPPED = 'Stopped';
}

class SocialMedia {
    static ALL = 'All';
    static FACEBOOK = 'Facebook';
    static TWITTER = 'Twitter';
    static INSTAGRAM = 'Instagram';
}

class Notification {
    static PATRON_REACTION = "gallery/notifications/reactions";
    static AUDIO_VOLUME = "gallery/notifications/audio/volume";
    static AUDIO_FILE = "gallery/notifications/audio/file";
    static IMAGE_TRANSITION = "gallery/notifications/images";
    static SPONSOR = "gallery/notifications/sponsors";
    static CURRENT_EVENT_ATTENDANCE = "console/notifications/events/ongoing/attendance";
    static SHOUTOUT = "console/notifications/shoutouts";
    static EVENT_STARTED = "gallery/notifications/events/current/started";
    static STREAMING = "console/notifications/streaming";
    static GROUP_STREAMING = "notifications/streaming/groups";
}

class URL {
    static AZURE_BLOB = "https://vfanx.blob.core.windows.net"
}

class Streaming {
    static CONFIG = {
        protocol: 'wss',
        host: 'live.vfanx.com',
        port: 443,
        app: 'live',
        rtcConfiguration: {
            iceServers: [{ urls: 'stun:stun2.l.google.com:19302' }],
            iceCandidatePoolSize: 2,
            bundlePolicy: 'max-bundle'
        },
        mediaElementId: 'red5pro-subscriber',
        muteOnAutoplayRestriction: true,
        //mediaConstraints: {
        //    audio: false,
        //    video: {
        //        width: {
        //            exact: 640
        //        },
        //        height: {
        //            exact: 480
        //        }
        //    }
        //},
        subscriptionId: 'mystream' + Math.floor(Math.random() * 0x10000).toString(16),
        videoEncoding: 'NONE',
        audioEncoding: 'NONE'
    };
}