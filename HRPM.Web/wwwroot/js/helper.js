﻿class HttpHelper {

    Get(url, success, error, token) {
        var request = new XMLHttpRequest()

        request.open('GET', url, true)

        //if (!token)
           // token = localStorage.getItem(Constants.API_TOKEN);
        //debugger;
       // request.setRequestHeader('Authorization', 'Bearer ' + token);
       // debugger; 
        request.onload = function () {
            success(this.response === '' ? null : this.response);
        }

        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 401) {
                //localStorage.removeItem(Constants.API_TOKEN);
            }
        }

        request.onerror = function () {
            if (error)
                error();
        }

        request.send();
    }

    Post(url, data, success, failure) {
        var request = new XMLHttpRequest();

        request.open("POST", url);

        request.setRequestHeader("Content-Type", "application/json");

        //let token = localStorage.getItem(Constants.API_TOKEN);
        //if (token)
           // request.setRequestHeader('Authorization', 'Bearer ' + token);

        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                if (request.status == 200)
                    success(this.response);

                else if (request.status == 401) {
                    //localStorage.removeItem(Constants.API_TOKEN);
                }

                else if (request.status == 400) {
                   // failure(this.response);
                }

                else if (request.status == 201) {
                    return (this.response);
                }
            }
        }

        if (data)
            request.send(data);

        else
            request.send();
    }

    Put(url, data, success, failure) {
        var request = new XMLHttpRequest()

        request.open('PUT', url, true)

        request.setRequestHeader("Content-Type", "application/json");

        //let token = localStorage.getItem(Constants.API_TOKEN);

        //if (token)
          //  request.setRequestHeader('Authorization', 'Bearer ' + token);

        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 200)
                success(this.response);

            else if (request.status == 401) {
                //localStorage.removeItem(Constants.API_TOKEN);
            }

            else if (request.readyState == 4 && request.status == 400) {
               // failure(this.response);
            }

        }

        if (data)
            request.send(data);

        else
            request.send();
    }

    Delete(url, success, failure) {
        var request = new XMLHttpRequest()

        request.open('DELETE', url, true)

        request.setRequestHeader("Content-Type", "application/json");

        let token = localStorage.getItem(Constants.API_TOKEN);

        if (token)
            request.setRequestHeader('Authorization', 'Bearer ' + token);

        request.onload = function () {
            success(this.response);
        }

        request.send();
    }
}

class AjaxHelper {
    Post(url, data, success, failure) {
        let token = localStorage.getItem(Constants.API_TOKEN);

        $.ajax({
            type: "POST",
            url: url,
            contentType: false,
            processData: false,
            data: data,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "bearer " + token);
            },
            success: function (result) {
                success(result);
            },
            error: function (xhr, status, p3, p4) {
                failure();
            }
        });
    }

    Delete(url, success, failure) {
        let token = localStorage.getItem(Constants.API_TOKEN);

        $.ajax({
            type: "DELETE",
            url: url,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "bearer " + token);
            },
            success: function (result) {
                success(result);
            },
            error: function (xhr, status, p3, p4) {
                failure();
            }
        });
    }
}

class FileHelper {
    isFileValid(fileName, validFileExtensions) {
        let fileNameParts = fileName.split(".");
        let extension = fileNameParts[fileNameParts.length - 1]

        return (validFileExtensions.indexOf(extension) !== -1);
    }
}

class ImageCompressionHelper {
    compressImages(selector, maxHeight, maxWidth) {
        let reTry = false;

        $(selector).each(function () {
            $(this).hide();

            if ($(this).attr("src") !== null && $(this).attr("src") !== undefined) {
                var ratio = 0;
                var width = $(this).width();
                var height = $(this).height();

                if (width < 1) {
                    console.log("Invalid Width. Attempting image compression again.");
                    reTry = true;
                }

                if (height < 1) {
                    console.log("Invalid Height. Attempting image compression again.");
                    reTry = true;
                }

                if (width > maxWidth) {
                    ratio = maxWidth / width;
                    $(this).css("width", maxWidth);
                    $(this).css("height", height * ratio);
                    height = height * ratio;
                    width = width * ratio;
                }

                if (height > maxHeight) {
                    ratio = maxHeight / height;
                    $(this).css("height", maxHeight);
                    $(this).css("width", width * ratio);
                    width = width * ratio;
                    height = height * ratio;
                }

                if (width > 0 && height > 0)
                    $(this).show();
            }
        });

        return reTry;
    }
}

let httpHelper = new HttpHelper();
let ajaxHelper = new AjaxHelper();
let fileHelper = new FileHelper();
let imageCompressionHelper = new ImageCompressionHelper();