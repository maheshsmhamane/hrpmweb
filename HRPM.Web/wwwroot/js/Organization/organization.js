﻿"use strict";

let organizationIdToDelete = 0;
let organizationIdToEdit = 0;


function saveOrganization() {
    if ($("#organization-name").val().trim().length === 0) {
        toastr.error("Please enter  Name.", { timeOut: 1000000 });
        return;
    }
   

    if ($("#organization-email").val().trim().length === 0) {
        toastr.error("Please enter email.");
        return;
    }
    //if (!validateEmail($("#organization-email").val()))  {
    //    toastr.error("Please enter Valid email.");
    //    return; }

    if ($("#organization-contact").val().trim().length === 0) {
        toastr.error("Please enter Contact.");
        return;
    }

    if ($("#organization-ownerName").val().trim().length === 0) {
        toastr.error("Please enter OwnerName .");
        return;
    }

    if ($("#organization-ownerContact").val().trim().length === 0) {
        toastr.error("Please enter OwnerContactNo.");
        return;
    }

    if ($("#organization-address").val().trim().length === 0) {
        toastr.error("Please enter Address.");
        return;
    }
    if ($("#organization-zipCode").val().trim().length === 0) {
        toastr.error("Please enter ZipCode.");
        return;
    }

    let userRequest = {
        "organizationName": $("#organization-name").val(),
        "email": $("#organization-email").val(),
        "contact": $("#organization-contact").val(),
        "ownerName": $("#organization-ownerName").val(),
        "ownerContact": $("#organization-ownerContact").val(),
        "address": $("#organization-address").val(),
        "zipCode": $('#organization-zipCode').val()
    }

    if (organizationIdToEdit > 0) {
        httpHelper.Put(API.Organization_API + "/" + organizationIdToEdit, JSON.stringify(userRequest), (data) => {
            toastr.info("Organization was updated successfully.");
            $("#add-organization-modal").modal('hide');
            getOrganizations();
        });

        organizationIdToEdit = 0;
        return;
    }


    httpHelper.Post(API.Organization_API, JSON.stringify(userRequest), (data) => {
        console.log("mydata",data);
        toastr.info("Organization was saved successfully.");
        $("#add-organization-modal").modal('hide');
        getOrganizations();
    });
}



function getOrganizations(data) {

   var  PageSize = $("#pages").val()
    var hasNext = data;
    var PageNumber = 1;
    var n = parseInt(localStorage.getItem("currentPage"));
    if (hasNext == 'next') {
        var t = parseInt(localStorage.getItem("totalPages"));
        if(n)
            PageNumber = n + 1;
        if(t)
        if (PageNumber > t) { PageNumber = 1; }
    }
    else if (hasNext =='previous')
    {
        if (n)
        PageNumber = n - 1;
        if (PageNumber <= 0) { PageNumber = 1;}
    }

    
    httpHelper.Get(API.Organization_API + '?PageSize=' + PageSize +'&PageNumber=' + PageNumber, (data) => {
        let usersdata = JSON.parse(data);

        let organizations = usersdata.organizations;
        let totalCount = usersdata.totalCount;
        let totalPages = usersdata.totalPages;
        let currentPage = usersdata.currentPage;
        localStorage.removeItem("currentPage");
        localStorage.removeItem("totalPages");
        localStorage.setItem("currentPage", currentPage);
        localStorage.setItem("totalPages", totalPages);

        let nextSize = (PageSize * currentPage); 
        let previousSize = (nextSize - PageSize)+1;
        let pageNavigation = previousSize + " to " + nextSize + " of " + totalPages;

        $("#has-next").text(usersdata.hasNext);
        $("#has-previous").text(usersdata.hasPrevious);
        $("#total-count").text(totalCount);
        $("#page-size").text(PageSize);
        $("#total-pages").text(totalPages);
        $("#page-navigation").text(pageNavigation);
        


        $("#organization-list tbody").empty();
        if (organizations.length === 0) {
            $(".msg-no-records").show();
            return;
        }
        $(".msg-no-records").hide();
        if (organizations.length > 0) {
            $("#organization-table").show();
        }
        organizations.forEach(org => {
            let row = "<tr>";
            row += "<td >" + "<a href=/Organization/OrganizationDetails/" + org.id + ">" + org.name + "</a>" + "</td >";// "<a href='/OrganizationDetails/" + org.id + ">"+org.name+"</a >" 
            row += "<td>" + (org.address ? org.address : "") + "</td>";
            row += "<td>" + (org.email ? org.email : "") + "</td>";
            row += "<td>" + (org.phone ? org.phone : "") + "</td>";
            row += "<td class='actions'>";
            row += "<span class='glyphicon glyphicon-pencil' onclick='setOrganizationToEdit(" + org.id +
                ",\"" + org.name +
                "\"," + "\"" + org.address + "\"" +
                "," + "\"" + org.email + "\"" +
                "," + "\"" + org.phone + "\"" +
                "," + "\"" + org.zipCode + "\"" +
                "," + "\"" + org.ownerContact + "\"" +
                "," + "\"" + org.ownerName + "\"" +
                ")'></span> ";
            row += "<span class='glyphicon glyphicon-trash' data-toggle='modal' data-target='#delete-organization-modal' onclick='setOrganizationToDelete(" + org.id + ")'></span>";
            row += "</td>";
            row += "</tr>";
            $("#organization-list tbody").append(row);
        });
    }, () => {

    });
}



function deleteOrganization() {
    httpHelper.Delete(API.Organization_API + "/" + organizationIdToDelete, (data) => {
        toastr.info("Organization was deleted successfully.");
        getOrganizations();
    }, () => {

    });
}

function setOrganizationToDelete(id) {
    organizationIdToDelete = id;
}

function setOrganizationToEdit(id, name, address, email, phone, zipCode, ownerContact,ownerName) {
    organizationIdToEdit = id;
    $("#add-organization-modal").modal();
    $("#organization-name").val(name),
        $("#organization-email").val(email),
        $("#organization-contact").val(phone),
        $("#organization-ownerName").val(ownerName),
        $("#organization-ownerContact").val(ownerContact),
        $("#organization-address").val(address),
        $('#organization-zipCode').val(zipCode),
        $("#save-edit-button").html("Update Organization")
}

function openAddOrganizationModal() {
    $("#add-organization-modal").modal();
     $("#organization-name").val(""),
        $("#organization-email").val(""),
             $("#organization-contact").val(""),
                 $("#organization-ownerName").val(""),
                   $("#organization-ownerContact").val(""),
                   $("#organization-address").val(""),
        $('#organization-zipCode').val(""),
    $("#save-edit-button").html("Add Organization")
    //document.getElementById("user-is-non-admin").checked = true;
}



//+"@Html.ActionLink(" + org.name ? org.name + '",Organization"'++'",OrganizationDetails"'"'new { id = " + org.id + " })"'+"</td >";
// <a asp-action='OrganizationDetails' asp-controller='Organization' asp-route-id='" + org.id + "'>" + (org.name ? org.name : "")
