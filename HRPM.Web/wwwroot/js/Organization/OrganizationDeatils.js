﻿"use strict";

function getUsers(organizationId, pagedata) {
    var PageSize = $("#userpages").val();
    var Status = '';
    Status = $("#status").val();
    var hasNext = pagedata;
    var PageNumber = 1;
    var n = parseInt(localStorage.getItem("currentPage"));
    if (hasNext == 'next') {
        var t = parseInt(localStorage.getItem("totalPages"));
        if (n)
            PageNumber = n + 1;
        if (t)
            if (PageNumber > t) { PageNumber = 1; }
    }
    else if (hasNext == 'previous') {
        if (n)
            PageNumber = n - 1;
        if (PageNumber <= 0) { PageNumber = 1; }
    }


    httpHelper.Get(API.User_API + organizationId + '/users/' + '?PageSize=' + PageSize + '&PageNumber=' + PageNumber + '&Status=' + Status, (data) => {
        let usersdata = JSON.parse(data);

        let users = usersdata.users;
        if (!users) { return; }
        let totalCount = usersdata.totalCount;
        let totalPages = usersdata.totalPages;
        let currentPage = usersdata.currentPage;
        localStorage.removeItem("currentPage");
        localStorage.removeItem("totalPages");
        localStorage.setItem("currentPage", currentPage);
        localStorage.setItem("totalPages", totalPages);

        let nextSize = (PageSize * currentPage);
        let previousSize = (nextSize - PageSize) + 1;
        let pageNavigation = previousSize + " to " + nextSize + " of " + totalPages;

        $("#has-next").text(usersdata.hasNext);
        $("#has-previous").text(usersdata.hasPrevious);
        $("#total-count").text(totalCount);
        $("#page-size").text(PageSize);
        $("#total-pages").text(totalPages);
        $("#userpage-navigation").text(pageNavigation);


        $("#user-list tbody").empty();
        if (users.length === 0) {
            $(".msg-no-records").show();
            return;
        }

        $(".msg-no-records").hide();
        if (users.length > 0) {
            $("#user-table").show();
        }

        users.forEach(usr => {
            let row = "<tr>";
            row += "<td >" +"<a href=/user/userdetails/" + usr.userId + ">" + usr.name + "</a>" + "</td >";
            row += "<td>" + (usr.email ? usr.email : "") + "</td>";
            row += "<td>" + (usr.phone ? usr.phone : "") + "</td>";
            row += "<td>" + (usr.role ? usr.role : "") + "</td>";
            if (usr.status == 'Active') { row += "<td class='statusActive'>" + (usr.status ? usr.status : "") + "</td>"; }
            if (usr.status == 'InActive') row += "<td class='statusInActive'>" + (usr.status ? usr.status : "") + "</td>"; 
            row += "</tr>";
            $("#user-list tbody").append(row);
        });

    }, () => {

    });

}

function getPatients(organizationId, pagedata) {
    var PageSize = $("#patientpages").val()
    var Status = '';
    Status = $("#status").val();
    var hasNext = pagedata;
    var PageNumber = 1;
    var n = parseInt(localStorage.getItem("currentPage"));
    if (hasNext == 'next') {
        var t = parseInt(localStorage.getItem("totalPages"));
        if (n)
            PageNumber = n + 1;
        if (t)
            if (PageNumber > t) { PageNumber = 1; }
    }
    else if (hasNext == 'previous') {
        if (n)
            PageNumber = n - 1;
        if (PageNumber <= 0) { PageNumber = 1; }
    }


    httpHelper.Get(API.User_API + organizationId + '/patients/' + '?PageSize=' + PageSize + '&PageNumber=' + PageNumber + '&Status=' + Status, (data) => {
        let usersdata = JSON.parse(data);

        let patients = usersdata.patients;
        if (!patients) { return; }
        let totalCount = usersdata.totalCount;
        let totalPages = usersdata.totalPages;
        let currentPage = usersdata.currentPage;
        localStorage.removeItem("currentPage");
        localStorage.removeItem("totalPages");
        localStorage.setItem("currentPage", currentPage);
        localStorage.setItem("totalPages", totalPages);

        let nextSize = (PageSize * currentPage);
        let previousSize = (nextSize - PageSize) + 1;
        let pageNavigation = previousSize + " to " + nextSize + " of " + totalPages;

        $("#has-next").text(usersdata.hasNext);
        $("#has-previous").text(usersdata.hasPrevious);
        $("#total-count1").text(totalCount);
        $("#page-size1").text(PageSize);
        $("#total-pages").text(totalPages);
        $("#patientpage-navigation").text(pageNavigation);


        $("#patient-list tbody").empty();
        if (patients.length === 0) {
            $(".msg-no-records").show();
            return;
        }
        $(".msg-no-records").hide();
        if (patients.length > 0) {
            $("#patient-table").show();
        }

        patients.forEach(usr => {
            let row = "<tr>";
            row += "<td >" + "<a href=/patient/patientdetails/" + usr.patientId + ">" + usr.name + "</a>" + "</td >";
            row += "<td>" + (usr.address ? usr.address : "") + "</td>";
            row += "<td>" + (usr.gender ? usr.gender : "") + "</td>";
            row += "<td>" + (usr.organizationName ? usr.organizationName : "") + "</td>";
            row += "<td>" + (usr.user ? usr.user : "") + "</td>";
            if (usr.status == 'Active') { row += "<td class='statusActive'>" + (usr.status ? usr.status : "") + "</td>"; }
            if (usr.status == 'InActive') row += "<td class='statusInActive'>" + (usr.status ? usr.status : "") + "</td>"; 
            row += "</tr>";
            $("#patient-list tbody").append(row);
        });

    }, () => {

    });

}
function getData(organizationId, pagedata) {
    if ($("#patientab").hasClass('active'));
    {
        getUsers(organizationId, pagedata)
    }
     if ($("#patientab").hasClass('active'));
    {
        getPatients(organizationId, pagedata);
    }
}