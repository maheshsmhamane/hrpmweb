﻿using HRPM.Contracts;
using HRPM.Entities;
using HRPM.Entities.Extensions;
using HRPM.Entities.Helpers;
using HRPM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace HRPM.Repository
{
   public  class PatientDeviceAssignmentService: RepositoryBase<PatientDeviceAssignment>, IPatientDeviceAssignmentRepository
    {
        public PatientDeviceAssignmentService(AppDbContext context) : base(context)
        {

        }

        public void CreatePatientDeviceAssignment(PatientDeviceAssignment patientDeviceAssignment)
        {
            Create(patientDeviceAssignment);
        }

        public void DeletePatientDeviceAssignment(PatientDeviceAssignment patientDeviceAssignment)
        {
            Delete(patientDeviceAssignment);
        }
        //public PagedList<Account> GetPatientByOwner(Guid ownerId, AccountParameters parameters)
        //{
        //    var accounts = FindByCondition(a => a.OwnerId.Equals(ownerId));

        //    return PagedList<Account>.ToPagedList(accounts,
        //        parameters.PageNumber,
        //        parameters.PageSize);
        //}

        //public Account GetAccountByOwner(Guid ownerId, Guid id)
        //{
        //    return FindByCondition(a => a.OwnerId.Equals(ownerId) && a.Id.Equals(id)).SingleOrDefault();
        //}
        public PagedList<PatientDeviceAssignment> GetPatientDeviceAssignment(int PatientId,PatientDeviceAssignmentParameters patientDeviceAssignmentParameters)
        {
            var patients = FindByCondition(a => a.PatientId.Equals(PatientId));
            return PagedList<PatientDeviceAssignment>.ToPagedList(patients,
                patientDeviceAssignmentParameters.PageNumber,
                patientDeviceAssignmentParameters.PageSize);
        }

        public PatientDeviceAssignment GetPatientDeviceAssignmentById(int PatientId,int PatientDeviceAssignmentId)
        {
            return FindByCondition(a => a.PatientId.Equals(PatientId) && a.Id.Equals(PatientDeviceAssignmentId)).SingleOrDefault();
        }

        public void UpdatePatientDeviceAssignment(PatientDeviceAssignment DbPatientDeviceAssignment, PatientDeviceAssignment patientDeviceAssignment)
        {
            DbPatientDeviceAssignment.Map(patientDeviceAssignment);
            Update(DbPatientDeviceAssignment);
        }
    }
}
