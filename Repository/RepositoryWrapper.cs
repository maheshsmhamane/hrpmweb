﻿using HRPM.Contracts;
using HRPM.Entities;

namespace HRPM.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private AppDbContext _repoContext;
        private IOrganizationRepository _organization;
        private IPatientRepository _patient;
        private IDeviceRepository _device;
        private IPatientDeviceAssignmentRepository _PatientDeviceAssignment;
        private IPatientHealthReadingRepository _patientHealthReading;
        private IUserRepository _user;
        private IRoleRepository _role;
        
        public IDeviceRepository Device
        {
            get
            {
                if (_device == null)
                {
                    _device = new DeviceService(_repoContext);
                }

                return _device;
            }
        }
        public IOrganizationRepository Organization
        {
            get
            {
                if (_organization == null)
                {
                    _organization = new OrganizationService(_repoContext);
                }

                return _organization;
            }
        }
        public IPatientRepository Patient
        {
            get
            {
                if (_patient == null)
                {
                    _patient = new PatientService(_repoContext);
                }

                return _patient;
            }
        }
        public IPatientDeviceAssignmentRepository PatientDeviceAssignment
        {
            get
            {
                if (_PatientDeviceAssignment == null)
                {
                    _PatientDeviceAssignment = new PatientDeviceAssignmentService(_repoContext);
                }

                return _PatientDeviceAssignment;
            }
        }
        public IPatientHealthReadingRepository PatientHealthReading
        {
            get
            {
                if (_patientHealthReading == null)
                {
                    _patientHealthReading = new PatientHealthReadingService(_repoContext);
                }

                return _patientHealthReading;
            }
        }

        public IUserRepository User
        {
            get
            {
                if (_user == null)
                {
                    _user = new UserService(_repoContext);
                }

                return _user;
            }
        }
        public IRoleRepository Role
        {
            get
            {
                if (_role == null)
                {
                    _role = new RoleService(_repoContext);
                }

                return _role;
            }
        }


        public RepositoryWrapper(AppDbContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }

        public void Save()
        {
            _repoContext.SaveChanges();
        }
    }
}
