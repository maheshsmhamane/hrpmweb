﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRPM.Contracts;
using HRPM.Entities;
using HRPM.Entities.Extensions;
using HRPM.Entities.Helpers;
using HRPM.Entities.Models;

using Microsoft.EntityFrameworkCore;

namespace HRPM.Repository
{
    public class PatientService : RepositoryBase<Patient>, IPatientRepository
    {
        public PatientService(AppDbContext context) : base(context)
        {

        }

        public void CreatePatient(Patient patients)
        {
            Create(patients);
        }

        public void DeletePatient(Patient patients)
        {
            Delete(patients);
        }

        public PagedList<PatientsViewModel> GetPatient(PatientParameters patientParameters)
        {
            //var patients = FindAll();
            var patients = (from P in AppDbContext.Patients
                         join O in AppDbContext.Organizations on P.OrganizationId equals O.Id
                         select new PatientsViewModel
                         {
                             PatientId = P.Id,
                             Name = P.GetFullName(),//patient Name
                             Address = P.Address,
                             Mobile = P.Mobile,
                             Gender = P.Gender,
                             OrganizationName = O.OrganizationName,
                             Status = P.StatusTypeEnum,
                             OrganizationId=P.OrganizationId,
                             UserId=P.UserId,
                         }).ToList();

            SearchByName(ref patients, patientParameters.Name);
            SearchByStatus(ref patients, patientParameters.Status);
            if (patientParameters.Orgnization != "All Orgnizations")
                SearchByOrganizationName(ref patients, patientParameters.Orgnization); //search Patients by organization name and also include all organizations
          

            return PagedList<PatientsViewModel>.ToPagedList(patients.OrderBy(on => on.Name),
                patientParameters.PageNumber,
                patientParameters.PageSize);

        }

        public PagedList<PatientsViewModel> GetPatient(int organizationId, PatientParameters patientParameters)
        {
            //var patients = FindAll();
            var patients = (from P in AppDbContext.Patients
                            join U in AppDbContext.Users on P.UserId equals U.Id
                            join O in AppDbContext.Organizations on P.OrganizationId equals O.Id
                            select new PatientsViewModel
                            {
                                PatientId = P.Id,
                                Name = P.GetFullName(),//patient Name
                                Address = P.Address,
                                Mobile = P.Mobile,
                                Gender = P.Gender,
                                OrganizationName = O.OrganizationName,
                                User=U.Name,
                                Status = P.StatusTypeEnum,
                                OrganizationId = P.OrganizationId,
                                UserId = P.UserId,
                            }).ToList();

            SearchByName(ref patients, patientParameters.Name);
            SearchByStatus(ref patients, patientParameters.Status);
            if (patientParameters.Orgnization != "All Orgnizations")
                SearchByOrganizationName(ref patients, patientParameters.Orgnization); //search Patients by organization name and also include all organizations


            return PagedList<PatientsViewModel>.ToPagedList(patients.OrderBy(on => on.Name),
                patientParameters.PageNumber,
                patientParameters.PageSize);

        }

        private void SearchByOrganizationName(ref List<PatientsViewModel> users, string orgname)
        {
            if (!users.Any() || string.IsNullOrWhiteSpace(orgname))
                return;

            users = users.Where(o => o.OrganizationName.ToLower() == (orgname.Trim().ToLower())).ToList();
        }
        private void SearchByName(ref List<PatientsViewModel> owners, string ownerName)
        {
            if (!owners.Any() || string.IsNullOrWhiteSpace(ownerName))
                return;

            owners = owners.Where(o => o.Name.ToLower().Contains(ownerName.Trim().ToLower())).ToList();
        }
        private void SearchByStatus(ref List<PatientsViewModel> owners, string status)
        {
            if (!owners.Any() || string.IsNullOrWhiteSpace(status))
                return;
                owners = owners.Where(o => o.Status.ToLower()==(status.Trim().ToLower())).ToList();
        }   



        public Patient GetPatientById(int patientId)
        {
            return FindByCondition(e => e.Id.Equals(patientId))
                .FirstOrDefault();
        }

        
        public void UpdatePatient(Patient dbpatients, Patient patients)
        {

            dbpatients.Map(patients);
            Update(dbpatients);
        }
    }
}
