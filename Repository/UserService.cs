﻿using HRPM.Contracts;
using HRPM.Entities;
using HRPM.Entities.Extensions;
using HRPM.Entities.Helpers;
using HRPM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace HRPM.Repository
{
    public class UserService : RepositoryBase<User>, IUserRepository
    {
        public UserService(AppDbContext context) : base(context)
        {

        }

        public void CreateUser(User user)
        {
            Create(user);
        }

        public void DeleteUser(User user)
        {
            Delete(user);
        }

        // return all users by specific organization by organiztion id inner join
        public PagedList<UserViewModel> GetUser(int OrgnizationId,UserParameters userParameters)
        {
            var users = (from U in AppDbContext.Users
                        join R in AppDbContext.Roles on U.RoleId equals R.Id
                        join O in AppDbContext.Organizations on U.OrganizationId equals O.Id
                        where O.Id ==OrgnizationId
 
                        select new UserViewModel
                        {
                            UserId = U.Id,
                            Name = U.Name,
                            Address = U.Address,
                            Email = U.Email,
                            Phone = U.Mobile,
                            Role = R.RoleType,
                            OrganizationName = O.OrganizationName,
                            Status = U.StatusTypeEnum

                        }).ToList();
            SearchByName(ref users, userParameters.Name);
            SearchByStatus(ref users, userParameters.Status);
            //Console.WriteLine(users.FirstOrDefault(x=>x.Status==userParameters.Status));

            return PagedList<UserViewModel>.ToPagedList(users.OrderBy(on => on.Name),
                userParameters.PageNumber,
                userParameters.PageSize);
        }

        // return all users from all organizationsby using Left join 
        public PagedList<UserViewModel> GetUser(UserParameters userParameters)
        {
            var users = (from U in AppDbContext.Users
                        join R in AppDbContext.Roles on U.RoleId equals R.Id
                        join O in AppDbContext.Organizations on U.OrganizationId equals O.Id
                        into OrgDetails
                         from m in OrgDetails.DefaultIfEmpty()
                         select new UserViewModel
                        {
                            UserId = U.Id,
                            Name = U.Name,
                            Address = U.Address,
                            Email = U.Email,
                            Phone = U.Mobile,
                            Role = R.RoleType,
                            OrganizationName = m != null ? m.OrganizationName : "NA",
                            OrganizationId=m!=null ? m.Id: 0,
                            Status = U.StatusTypeEnum,
                            RoleId=R.Id
                        }).ToList();

            SearchByName(ref users, userParameters.Name);//serch  user by user name
            if(userParameters.Organization!= "All Organizations")
            SearchByOrganizationName(ref users, userParameters.Organization); //search organization by organization name and also include all organizations
            SearchByStatus(ref users, userParameters.Status);// serch user by sttus of user Active Inactive 
             return PagedList<UserViewModel>.ToPagedList(users.OrderBy(on => on.Name),
                userParameters.PageNumber,
                userParameters.PageSize);
        }

        private void SearchByOrganizationName(ref List<UserViewModel> users, string orgname)
        {
            if (!users.Any() || string.IsNullOrWhiteSpace(orgname))
                return;

            users = users.Where(o => o.OrganizationName.ToLower() == (orgname.Trim().ToLower())).ToList();
        }

        private void SearchByName(ref List<UserViewModel> owners, string ownerName)
        {
            if (!owners.Any() || string.IsNullOrWhiteSpace(ownerName))
                return;

            owners = owners.Where(o => o.Name.ToLower().Contains(ownerName.Trim().ToLower())).ToList();
        }

        //search by status by using LIST Instead of Iquarble getting runtime exception  due to EFCore 3.0 above issue
        private void SearchByStatus(ref List<UserViewModel> users, string status)
        {
            if (!users.Any() || string.IsNullOrWhiteSpace(status))
                return;

            users = users.Where(o => o.Status.ToLower()==(status.Trim().ToLower())).ToList();
        }   

        public User GetUserById(int organizationId,int userId)
        {
            return FindByCondition(a => a.OrganizationId==(organizationId) && a.Id==(userId)).SingleOrDefault();

        }
        public User GetUserById(int userId)
        {
            return FindByCondition(a=> a.Id == (userId)).SingleOrDefault();

        }

        public void UpdateUser(User Dbuser, User user)
        {
            Dbuser.Map(user);
            Update(Dbuser);
        }
    }
}
