﻿using HRPM.Contracts;
using HRPM.Entities;
using HRPM.Entities.Extensions;
using HRPM.Entities.Helpers;
using HRPM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace HRPM.Repository
{
   public  class RoleService: RepositoryBase<Role>, IRoleRepository
    {
        public RoleService(AppDbContext context) : base(context)
        {

        }

        public void CreateRole(Role role)
        {
            Create(role);
        }

        public void DeleteRole(Role role)
        {
            Delete(role);
        }

        public PagedList<Role> GetRole(PagingParameters pageParameters)
        {

            return PagedList<Role>.ToPagedList(FindAll(),
                pageParameters.PageNumber,
                pageParameters.PageSize);

        }
        private void SearchByName(ref IQueryable<User> owners, string ownerName)
        {
            if (!owners.Any() || string.IsNullOrWhiteSpace(ownerName))
                return;

            owners = owners.Where(o => o.Name.ToLower().Contains(ownerName.Trim().ToLower()));
        }

        public Role GetRoleById(int RoleId)
        {

            return FindByCondition(Role => Role.Id.Equals(RoleId))
                 .FirstOrDefault();
        }

        public void UpdateRole(Role DbRole, Role role)
        {

            DbRole.Map(role);
            Update(DbRole);
        }
    }
}
