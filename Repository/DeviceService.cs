﻿using HRPM.Contracts;
using HRPM.Entities;
using HRPM.Entities.Extensions;
using HRPM.Entities.Helpers;
using HRPM.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HRPM.Repository
{

    public class DeviceService :RepositoryBase<Device>, IDeviceRepository
    {

        public DeviceService(AppDbContext context) : base(context)
        {

        }

       
        public PagedList<Device> GetDevice(DeviceParameters deviceParameters)
        {
            var owners = FindAll();
            SearchByName(ref owners, deviceParameters.Name);
            return PagedList<Device>.ToPagedList(owners.OrderBy(on => on.DeviceName),
                deviceParameters.PageNumber,
                deviceParameters.PageSize);


            //var owners = FindByCondition(o => o.DateOfBirth.Year >= ownerParameters.MinYearOfBirth &&
            //                            o.DateOfBirth.Year <= ownerParameters.MaxYearOfBirth);

            //SearchByName(ref owners, ownerParameters.Name);

            //return PagedList<Owner>.ToPagedList(owners.OrderBy(on => on.Name),
            //    ownerParameters.PageNumber,
            //    ownerParameters.PageSize);

        }
        private void SearchByName(ref IQueryable<Device> owners, string ownerName)
        {
            if (!owners.Any() || string.IsNullOrWhiteSpace(ownerName))
                return;

            owners = owners.Where(o => o.DeviceName.ToLower().Contains(ownerName.Trim().ToLower()));
        }

        public Device GetDeviceById(int deviceId)
        {
            var data= FindByCondition(Device => Device.Id.Equals(deviceId))
                .FirstOrDefault();
            return data;
        }

        public void CreateDevice(Device device)
        {
            Create(device);
        }

        public void UpdateDevice(Device dbDevice, Device device)
        {
            dbDevice.Map(device);
            Update(dbDevice);
        }

        public void DeleteDevice(Device device)
        {
            Delete(device);
        }
    }
}
