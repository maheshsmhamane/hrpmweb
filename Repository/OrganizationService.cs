﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRPM.Contracts;
using HRPM.Entities;
using HRPM.Entities.Extensions;
using HRPM.Entities.Helpers;
using HRPM.Entities.Models;
using Microsoft.EntityFrameworkCore;


namespace HRPM.Repository
{
    public class OrganizationService:RepositoryBase<Organization>,   IOrganizationRepository
    {
        public OrganizationService(AppDbContext context) : base(context)
        {

        }
        public PagedList<OrganizationViewModel> GetOrganization(OrganizationParameters deviceParameters)
        {
            var organizations = FindAll();
           
           var orgViewModel =organizations.Select(x => new OrganizationViewModel
            {
                Id = x.Id,
                Name = x.OrganizationName,
                Address = x.Address,
                Email=x.Email,
                phone=x.Contact,
                ZipCode=x.ZipCode,
                OwnerContact=x.OwnerContact,
                OwnerName=x.OwnerName
            });
            SearchByName(ref organizations, deviceParameters.Name);
            


            return PagedList<OrganizationViewModel>.ToPagedList(orgViewModel.OrderBy(on => on.Name),
                deviceParameters.PageNumber,
                deviceParameters.PageSize);

        }

        private void SearchByName(ref IQueryable<Organization> owners, string ownerName)
        {
            if (!owners.Any() || string.IsNullOrWhiteSpace(ownerName))
                return;

            owners = owners.Where(o => o.OrganizationName.ToLower().Contains(ownerName.Trim().ToLower()));
        }

        public Organization GetOrganizationById(int OganizationId)
        {
            return FindByCondition(e => e.Id.Equals(OganizationId))
                .FirstOrDefault();
        }

        public void CreateOrganization(Organization organization)
        {
            Create(organization);
        }

        public void UpdateOrganization(Organization Dborganization, Organization organization)
        {
            Dborganization.Map(organization);
            Update(Dborganization);
        }

        public void DeleteOrganization(Organization organization)
        {
            Delete(organization);
        }

        
    }
}
