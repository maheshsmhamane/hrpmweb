﻿using HRPM.Contracts;
using HRPM.Entities;
using HRPM.Entities.Extensions;
using HRPM.Entities.Helpers;
using HRPM.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace HRPM.Repository
{
    public class PatientHealthReadingService : RepositoryBase<PatientHealthReading>, IPatientHealthReadingRepository
    {
        public PatientHealthReadingService(AppDbContext context) : base(context)
        {

        }

        public void CreatePatientHealthReading(PatientHealthReading patientHealthReading)
        {
            Create(patientHealthReading);
        }

        public void DeletePatientHealthReading(PatientHealthReading patientHealthReading)
        {
            Delete(patientHealthReading);
        }

        public PagedList<PatientHealthReading> GetPatientHealthReading(int deviceId, PagingParameters pageParameters)
        {
            var healthReadings = FindByCondition(a => a.DeviceId.Equals(deviceId));
            return PagedList<PatientHealthReading>.ToPagedList(healthReadings,
                pageParameters.PageNumber,
                pageParameters.PageSize);

        }
        //
        //    return PagedList<PatientDeviceAssignment>.ToPagedList(patients,
        //        patientDeviceAssignmentParameters.PageNumber,
        //        patientDeviceAssignmentParameters.PageSize);
        //}



        public PatientHealthReading GetPatientHealthReadingById(int deviceid, int patientHealthId)
        {
            return FindByCondition(a => a.DeviceId.Equals(deviceid) && a.Id.Equals(patientHealthId)).SingleOrDefault();
        }

        public void UpdatePatientHealthReading(PatientHealthReading DbpatientHealthReading, PatientHealthReading patientHealthReading)
        {
            throw new NotImplementedException();
        }
    }
}
